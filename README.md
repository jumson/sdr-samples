# sdr-samples

This is a place for placing raw sample files captured using various SDRs, for various devices of interest.

The whole repo has grown to about 4.4 GB

Use the following URL pattern to download any particular iq file:

```bash
wget https://gitlab.com/jumson/sdr-samples/-/raw/master/FOLDER_NAME/FILE_NAME
```

For example, the `HackRF_GarageDoor_315_8.iq` file in the `garage_door_opener` directory is 397M large.
```
wget https://gitlab.com/jumson/sdr-samples/-/raw/master/garage_door_opener/HackRF_GarageDoor_315_8.iq
```

Also, take note in my naming convention (perhaps not followed 100%...)

I might have the name of the device that recorded it (above, that was a HackRF), the name of the device it was recording (above, being a garage door opener), the center frequency (315MHz), and the sample rate (8 million samples per second).

I didn't follow this 100% it seems (writing this a few years later...) but there should be enough clues to figure it out.
